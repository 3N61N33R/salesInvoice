import functools
import logging
import operator

from django.db.models import Q
from django_filters import rest_framework as filters
import datetime
from .models import SalesInvoice
from django.utils.dateparse import parse_date


logger = logging.getLogger(__name__)
NULL_VALUE = "unspecified"


class SalesInvoiceFilter(filters.FilterSet):
    """Filters sales invoice data by specified fields"""

    # invoice_date = filters.DateFromToRangeFilter(field_name='invoice_date')
    date_before = filters.DateFilter(
        field_name='person_sessions__start_time', lookup_expr=('gt'),)
    date_after = filters.DateFilter(
        field_name='person_sessions__start_time', lookup_expr=('lt'))

    invoice_date = filters.CharFilter(field_name='invoice_date',
                                      method='filter_exact_with_multiple_query_values',
                                      lookup_expr='iexact',
                                      )

    def filter_exact_with_multiple_query_values(self, queryset, name, value):
        options = set(value.split(","))
        print(options)
        null_lookup = {}
        if NULL_VALUE in options:
            options.remove(NULL_VALUE)
            null_lookup = {"__".join([name, "isnull"]): True}
        lookup = {"__".join([name, "in"]): options}
        return queryset.filter(Q(**lookup) | Q(**null_lookup))

    def filter_date_range(self, queryset, name, value):
        print(queryset, name, value)
        filter_dict = {}

        splitted = value.split(',')
        print(splitted
              )

        if len(splitted) != 2:
            return queryset

        start = parse_date(splitted[0])
        end = parse_date(splitted[1])

        filter_dict[name +
                    '__gte'] = start if start else datetime.date(2000, 1, 1)
        filter_dict[name + '__lt'] = end if end else datetime.date(2200, 1, 1)
        print(filter_dict)

        return queryset.filter(**filter_dict)

    class Meta:
        model = SalesInvoice
        fields = [
            'invoice_date',
        ]
