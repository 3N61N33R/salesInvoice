import tablib
from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import SalesInvoice

# Register your models here.

class SalesInvoiceResource(resources.ModelResource):
    class Meta:
        model = SalesInvoice

class SalesInvoiceAdmin(ImportExportModelAdmin):
    resource_class = SalesInvoiceResource

admin.site.register(SalesInvoice, SalesInvoiceAdmin)

