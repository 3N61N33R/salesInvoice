from rest_framework import serializers

from .models import SalesInvoice
from sales_invoice import settings


class SalesInvoiceSerializer(serializers.ModelSerializer):
    invoice_date = serializers.DateField(format=settings.DATE_FORMAT)
    due_date = serializers.DateField(format=settings.DATE_FORMAT)
    total_amount = serializers.IntegerField()

    class Meta:
        model = SalesInvoice
        fields = ['id', 'contact_name', 'invoice_number', 'invoice_date', 'due_date',
                  'description', 'quantity', 'unit_amount', 'account_code', 'total_amount',
                  ]

    # def to_representation(self, instance):
    #     representation = super(SalesInvoiceSerializer, self).to_representation(instance)
    #     representation['invoice_date'] = instance.invoice_date.strftime('%d/%m/%Y')
    #     return representation

    # def get_total_amount(self,quantity, unit_amount):
    #     """method to calculate total amount"""
    #     req = self.context.get('request')
    #     res = req.quantity * req.unit_amount
    #     return res
