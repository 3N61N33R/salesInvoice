from django.db import models
from datetime import datetime as dt
from .abstract_model import TimeStampedModel

# Create your models here.


class SalesInvoice(TimeStampedModel):
    """Sales Invoice Model"""
    contact_name = models.CharField(max_length=128)
    invoice_number = models.IntegerField(db_index=True)
    invoice_date = models.DateField(default=dt.now)
    due_date = models.DateField(default=dt.now)
    description = models.TextField()
    quantity = models.IntegerField()
    unit_amount = models.IntegerField()
    account_code = models.IntegerField()

    def __str__(self):
        """returns a string representation of the object - sales invoice"""
        return self.contact_name

    @property
    def total_amount(self):
        return self.quantity * self.unit_amount
