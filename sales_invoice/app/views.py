import csv
import io
import codecs
import calendar
import datetime
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.contrib import messages
from django.db.models import Sum
from django_filters import rest_framework as filters

from rest_framework.views import APIView, View
from rest_framework.parsers import MultiPartParser, FileUploadParser
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework import mixins, generics
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from .models import SalesInvoice
from .serializers import SalesInvoiceSerializer
from .csv_import_helper import DictReaderStrip, CSV_HEADERS, process_file, format_date
from .pagination_helper import LimitOffsetPagination
from .filters import SalesInvoiceFilter
# Create your views here.


class SalesInvoiceImportView(APIView):
    """Import sales invoice data"""
    serializer_class = SalesInvoiceSerializer
    parser_classes = [MultiPartParser]
    permission_classes = [AllowAny]
    queryset = SalesInvoice.objects.all()

    @csrf_exempt
    def post(self, request):
        file_object = request.data.get('file')
        if not file_object:
            return Response({
                'error': 'CSV file to import from not provided'
            }, status=400)

        if not file_object.name.endswith('.csv'):
            return Response({
                'error': 'file not supported, import a CSV file'
            })

        # file_obj = file_object.read().decode('UTF-8')
        file_obj = codecs.iterdecode(file_object, "utf-8")
        print(file_obj)
        csv_reader = DictReaderStrip(file_obj, delimiter=",")
        if not (csv_reader.fieldnames and " ".join(csv_reader.fieldnames).strip()):
            return Response({"error": "CSV file is empty"}, status=400)
        field_names_set = set(csv_reader.fieldnames)
        if not field_names_set.issubset(CSV_HEADERS):
            return Response({"error": "CSV file contains invalid headings"}, status=400)

        # file_obj = file_object.read().decode('UTF-8')
        # io_string = io.StringIO(file_obj)
        # next(io_string)

        # # import pdb; pdb.set_trace()
        # print('Processing file')
        # invoice_date = format_date(csv_reader.fieldnames[12])
        # due_date = format_date(csv_reader.fieldnames['*DueDate'])
        # print(invoice_date)

        for column in csv.reader(file_obj, delimiter=',', quotechar='"'):
            invoice_date = format_date(column[12])
            due_date = format_date(column[13])
            _, created = SalesInvoice.objects.update_or_create(
                contact_name=column[0],
                invoice_number=column[10],
                invoice_date=invoice_date,
                due_date=due_date,
                description=column[16],
                quantity=column[17],
                unit_amount=column[18],
                account_code=column[20],
            )
        response = {}
        # file_obj = codecs.iterdecode(file_object, "utf-8")
        # csv_reader = DictReaderStrip(file_obj, delimiter=",")
        # if process_file(csv_reader):
        #     response["success"] = "Scheme import completed successfully "
        # response["error "] = "Unable to import csv"
        response["success"] = "file import completed successfully "
        return Response(data=response, status=200)

    def get(self, request):
        data = request.data
        print(data)
        return Response(data)


class InvoiceData(ModelViewSet):
    queryset = SalesInvoice.objects.all()
    serializer_class = SalesInvoiceSerializer
    http_method_names = ['get', ]
    pagination_class = LimitOffsetPagination
    filterset_class = SalesInvoiceFilter

    def get_queryset(self):
        ''' method to filter invoice by date range'''
        queryset = SalesInvoice.objects.all()

        date = self.request.query_params.get('date')

        if date is None:
            return queryset
        start_date = format_date(date)
        next_date = start_date + datetime.timedelta(30)

        if start_date:
            queryset = queryset.filter(
                invoice_date__range=[start_date, next_date])
            if len(queryset) > 0:
                return queryset
        return queryset.filter(invoice_date='no result found')

    def filter_queryset(self, queryset):
        filter_backends = (filters.DjangoFilterBackend, )

        for backend in list(filter_backends):
            queryset = backend().filter_queryset(self.request, queryset, view=self)
        return queryset

    def list(self, request):
        ''' method to fetch all sales invoice data'''
        serializer_context = {'request': request}

        page = self.paginate_queryset(self.get_queryset())
        serializer = self.serializer_class(

            page, context=serializer_context, many=True)

        return self.get_paginated_response(serializer.data)

    @action(detail=False, methods=['GET'], name='Get Total')
    def total(self, request):
        """method to list sales invoice data by total amount"""
        queryset = SalesInvoice.objects.all()
        data = []
        for item in queryset:
            customer = item.contact_name
            total = item.total_amount
            obj = {"name": customer,
                   "total_amount": total}
            data.append(obj)
        sorted_data = sorted(
            data, key=lambda i: i['total_amount'], reverse=True)
        top = sorted_data[:5]

        return Response(top, status=200)


class InvoiceSummary(generics.ListAPIView):
    queryset = SalesInvoice.objects.all()
    serializer_class = SalesInvoiceSerializer

    def get(self, request):
        queryset = SalesInvoice.objects.all()
        data = {}

        for item in queryset:

            year = item.invoice_date.year
            month = calendar.month_name[item.invoice_date.month]
            total_amount = item.total_amount
            # this = SalesInvoice.objects.extra({'month': "to_char(day, 'Mon')", "year": "extract(year from day)"}).values(
            #     'month', 'year').annotate(Sum(total_amount))
            # print(this)

            _obj = {year:
                    {month: total_amount}
                    }
            try:
                data_ = data[year]

                try:
                    print(data_per_year[month])
                    monthly_totals = data_[month] + total_amount
                    data_.update({month: monthly_totals})
                except:
                    data_.update(_obj[year])
            except:
                data.update(_obj)

        return Response(data, status=200)
