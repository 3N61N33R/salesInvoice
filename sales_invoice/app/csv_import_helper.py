import csv
from rest_framework.validators import ValidationError
from datetime import datetime as dt
import datetime
from dateutil.parser import parse

CSV_HEADERS = {'*ContactName',
               'EmailAddress',
               'POAddressLine1',
               'POAddressLine2',
               'POAddressLine3',
               'POAddressLine4',
               'POCity',
               'PORegion',
               'POPostalCode',
               'POCountry',
               '*InvoiceNumber',
               'Reference',
               '*InvoiceDate',
               '*DueDate',
               'Total',
               'InventoryItemCode',
               '*Description',
               '*Quantity',
               '*UnitAmount',
               'Discount',
               '*AccountCode',
               '*TaxType',
               'TaxAmount',
               'TrackingName1',
               'TrackingOption1',
               'TrackingName2',
               'TrackingOption2',
               'Currency',
               'BrandingTheme'}


def format_date(date_):
    print(date_)
    try:
        parse(date_).date()
        try:
            dt.strptime(date_, '%d/%m/%Y')
        except ValueError:
            raise ValidationError(
                'Invalid date. Date must be of the format DD/MM/YYYY')
    except ValueError:
        raise ValidationError(
            'Invalid date. Date must be of the format DD/MM/YYYY')

    year, month, day = date_.split("/")[::-1]
    date = datetime.date(int(year), int(month), int(day))

    return date


def process_file(data, progress=None):
    for row_id, row in enumerate(data):
        row_data = {"row": row, "row_count": row_id}
        try:
            progress.update()
        except Exception:
            pass
        row_data['*ContactName'] = read_csv_row_value('*ContactName', row)
        row_data['EmailAddress', ] = read_csv_row_value('EmailAddress', row)
        row_data['POAddressLine1'] = read_csv_row_value('POAddressLine1', row)
        row_data['POAddressLine2'] = read_csv_row_value('POAddressLine2', row)
        row_data['POAddressLine3'] = read_csv_row_value('POAddressLine3', row)
        row_data['POAddressLine4'] = read_csv_row_value('POAddressLine4', row)
        row_data['POCity'] = read_csv_row_value('POCity', row)
        row_data['PORegion'] = read_csv_row_value('PORegion', row)
        row_data['POPostalCode'] = read_csv_row_value('POPostalCode', row)
        row_data['POCountry'] = read_csv_row_value('POCountry', row)
        row_data['*InvoiceNumber'] = read_csv_row_value(
            '*InvoiceNumber', row)
        row_data['Reference'] = read_csv_row_value('Reference', row)
        row_data['*InvoiceDate'] = read_csv_row_value('*InvoiceDate', row)
        row_data['*DueDate'] = read_csv_row_value('*DueDate', row)
        row_data['Total'] = read_csv_row_value('Total', row)
        row_data['InventoryItemCode'] = read_csv_row_value(
            'InventoryItemCode', row)
        row_data['*Description'] = read_csv_row_value('*Description', row)
        row_data['*Quantity'] = read_csv_row_value('*Quantity', row)
        row_data['*UnitAmount'] = read_csv_row_value('*UnitAmount', row)
        row_data['Discount'] = read_csv_row_value('Discount', row)
        row_data['*AccountCode'] = read_csv_row_value('*AccountCode', row)
        row_data['*TaxType'] = read_csv_row_value('*TaxType', row)
        row_data['TrackingName1'] = read_csv_row_value('TrackingName1', row)
        row_data['TrackingOption1'] = read_csv_row_value(
            'TrackingOption1', row)
        row_data['TrackingName2'] = read_csv_row_value('TrackingName2', row)
        row_data['TrackingOption2'] = read_csv_row_value(
            'TrackingOption2', row)
        row_data['Currency'] = read_csv_row_value('Currency', row)
        row_data['BrandingTheme'] = read_csv_row_value('BrandingTheme', row)


def read_csv_row_value(header_name, row):
    value = row.get(header_name)
    if value:
        return value.strip()
    return None


class DictReaderStrip(csv.DictReader):
    @property
    def fieldnames(self):
        if self._fieldnames is None:
            csv.DictReader.fieldnames.fget(self)
            if self._fieldnames is not None:
                self._fieldnames = [
                    name.strip() for name in self._fieldnames if name and name.strip()
                ]
        return self._fieldnames
