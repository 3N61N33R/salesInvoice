import React, { Component } from "react";

export default class Graph extends Component {
  state = {};
  renderLines() {
    return Array(10)
      .fill(null)
      .map((el, i) => <Line left={i * 10} key={i} />);
  }

  renderBars() {
    const { customers } = this.props;
    return customers.map(item => {
      const percent = item.total_amount / 100;
      return <Bar percent={percent} key={item.name} />;
    });
  }
  render() {
    return (
      <div className="graph-wrapper">
        <h2> {this.props.graphTitle} </h2>
        <div className="graph_">
          <BarTextContent customers={this.props.customers} />
          <div className="bar-lines-container">
            {this.renderLines()}
            {this.renderBars()}
          </div>

          <div style={{ width: "12%" }} />
          <Markers />
        </div>
      </div>
    );
  }
}

const Markers = () => {
  const markerArr = Array(11).fill(null);

  return (
    <div className="markers">
      {markerArr.map((el, i) => (
        <span className="marker" style={{ left: `${i * 10}%` }}>
          {i * 10}
        </span>
      ))}
    </div>
  );
};

const Bar = ({ percent }) => {
  return <div className="bar" style={{ width: `${percent}%` }} />;
};

const BarTextContent = ({ customers }) => {
  return (
    <div className="bar-text-content">
      {customers.map(item => (
        <div className="text">{item.name}</div>
      ))}
    </div>
  );
};

const Line = ({ left }) => {
  return <div className="line" style={{ left: `${left}%` }} />;
};
