import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Form from "./form";
import SummaryTable from "./table";
import axios from "axios";
import Nav from "./nav";
import Graph from "./barChat";

class App extends Component {
  state = {
    summary: {},
    customers: []
  };
  componentDidMount() {
    this.handleSummary();
    this.handleTopCustomers();
  }
  handleSummary = async () => {
    const response = await axios({
      method: "get",
      url: "https://secure-tor-65886.herokuapp.com/summary/",
      responseType: "stream"
    });
    this.setState({
      summary: response.data
    });
  };

  handleTopCustomers = async () => {
    const response = await axios({
      method: "get",
      url: "https://secure-tor-65886.herokuapp.com/data/total",
      responseType: "stream"
    });
    this.setState({
      customers: response.data
    });
  };

  render() {
    return (
      <div className="App">
        <Nav />
        <Form />
        <Graph
          customers={this.state.customers}
          graphTitle="Top 5 Customers by Total Amount"
        />
        <SummaryTable
          data={this.state.summary}
          tableTitle="Yearly Summary By Month"
        />
      </div>
    );
  }
}

export default App;
