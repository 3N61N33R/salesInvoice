import React from "react";
import { Button, Upload, Icon, message } from "antd";

const props = {
  name: "file",
  action: "https://secure-tor-65886.herokuapp.com/import/",
  method: "post",
  headers: {
    authorization: "authorization-text"
  },
  onChange(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  }
};

const Form = () => (
  <div className="App">
    <Upload {...props}>
      <Button>
        <Icon type="upload" />
        Click to Upload
      </Button>
    </Upload>
  </div>
);

export default Form;
