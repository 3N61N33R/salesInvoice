import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
    maxWidth: 800
  }
});

function createData(year, month, total) {
  return { year, month, total };
}

function table(rows) {
  return (
    <table>
      <tr>
        <th>Year</th>
        <th>Month</th>
        <th>Total</th>
      </tr>
      {rows.map(item => (
        <tr>
          <td>{item.year}</td>
          <td></td>
          <td></td>
        </tr>
      ))}
    </table>
  );
}

export default function SummaryTable(props) {
  const classes = useStyles();

  let year = [];

  if (!props.data) {
    return <div>'Fetching'</div>;
  }
  const years = [...year, ...Object.keys(props.data)];

  const rows = years.map(item => {
    return createData(
      item,
      Object.keys(props.data[item]),
      Object.values(props.data[item])
    );
  });

  return table(rows);
}
